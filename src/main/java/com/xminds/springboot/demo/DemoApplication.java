package com.xminds.springboot.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    @RestController
    public static class DemoController {

        @GetMapping("/")
        public HashMap<String, Object> hello(HttpServletRequest servletRequest, @RequestHeader Map<String, String> headers) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("time", LocalDateTime.now().toString());
            data.put("requestHeaders", headers);
            return data;
        }

    }
}
