FROM openjdk:8u111-jdk-alpine
WORKDIR /usr/src/app
COPY . .

RUN ./gradlew test && ./gradlew assemble
CMD java -jar build/libs/demo-0.0.1-SNAPSHOT.jar